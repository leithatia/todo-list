package org.academiadecodigo.proxymorons.todolist;

import java.util.PriorityQueue;

public class TodoList {

    PriorityQueue<TodoItem> queue;

    public TodoList() {
        queue = new PriorityQueue<TodoItem>();
    }

    public void add(Importance importance, int priority, String description) {
        TodoItem item = new TodoItem(importance, priority, description);

        queue.offer(item);
    }

    public TodoItem remove() {
        System.out.println(queue.poll());
        return queue.poll();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
