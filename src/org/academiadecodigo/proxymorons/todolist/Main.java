package org.academiadecodigo.proxymorons.todolist;

public class Main {
    public static void main(String[] args) {
        TodoList todoList = new TodoList();

        todoList.add(Importance.LOW, 2, "Blow my nose");
        todoList.add(Importance.HIGH, 1, "Make a game");
        todoList.add(Importance.MEDIUM, 1, "Bully my friend Alex");
        todoList.add(Importance.LOW, 1, "Squeeze the pig");
        todoList.add(Importance.MEDIUM, 2, "Get some sleep");
        todoList.add(Importance.HIGH, 2, "Breathe");


        while (!todoList.isEmpty()) {
            System.out.println(todoList.remove());
        }
    }

}
