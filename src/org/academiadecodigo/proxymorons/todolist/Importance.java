package org.academiadecodigo.proxymorons.todolist;

public enum Importance {
    HIGH,
    MEDIUM,
    LOW
}
