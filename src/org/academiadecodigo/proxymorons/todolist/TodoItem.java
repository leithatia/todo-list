package org.academiadecodigo.proxymorons.todolist;

public class TodoItem implements Comparable<TodoItem>{
    private final Importance importance;
    private final int priority;
    private final String description;

    public TodoItem (Importance importance, int priority, String description) {
        this.importance = importance;
        this.priority = priority;
        this.description = description;
    }

    public Importance getImportance() {
        return importance;
    }

    public int getPriority() {
        return priority;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Importance: " + getImportance() + " | Priority: " + getPriority() + " | " + getDescription();
    }

    @Override
    public int compareTo(TodoItem item) {
        if (this.importance.compareTo(item.importance) == 0) {
            return Integer.compare(this.priority, item.priority);
        }
        return this.importance.compareTo(item.importance);
    }
}
